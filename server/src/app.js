const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const basicAuth = require('./helpers/basic-auth');
const errorHandler = require('./helpers/error-handler');
const mysql      = require('mysql');


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

app.use(basicAuth);

const router = express.Router();
//Rotas
const index = require('./routes/index');
const funcionarioRoute = require('./routes/FuncionarioRoute');
const userRoute = require('./routes/UserRoute')

app.use('/', index);
app.use('/funcionarios', funcionarioRoute);
app.use('/user', userRoute);

app.use(errorHandler);


const connection = mysql.createConnection({
    host     : '172.19.0.2',
    user     : 'root',
    password : 'carterpie',
    database : 'funcionarios'
  });
	
connection.connect(function(err){
  if(err) return console.log(err);
  console.log('conectou!');
  createTable(connection)
})

function createTable(conn){
 
  const sql = "CREATE TABLE `funcionarios` ("+
    "`id` int(11) NOT NULL AUTO_INCREMENT,"+
    "`nome` varchar(80) DEFAULT NULL,"+
    "`sexo` char(1) DEFAULT NULL,"+
    "`dataNascimento` date DEFAULT NULL,"+
    "PRIMARY KEY (`id`)"+
    ")"
  
  
    try{
      conn.query(sql, function (error, results, fields){
        if(!error) console.log('criou a tabela!');
      });
    }
    catch(erro)
    {
      
    }
}

module.exports = app;
