const mysql      = require('mysql');

function execSqlCmd(query,res){
    
    const connection = mysql.createConnection({
        host     : '172.19.0.2',
        user     : 'root',
        password : 'carterpie',
        database : 'funcionarios'
      });
    connection.query(query,function(error,results,fields){
        if(results.fieldCount!=0)
            res.json(results);
        connection.end();
    });
}

exports.get = (req, res, next) => {
    execSqlCmd('select * from funcionarios',res);
};
exports.getById = (req, res, next) => {
    let id = req.params.id;
    execSqlCmd('select * from funcionarios where id='+id,res);
};
exports.post = (req, res, next) => {
    var func = req.body
    execSqlCmd(`insert into funcionarios(nome,sexo,dataNascimento) 
                values ('${func.nome}','${func.sexo}','${func.dataNascimento}')`,req)
    res.status(201).send('Inserido!');
};
exports.put = (req, res, next) => {
    let id = req.params.id;
    var func = req.body
    execSqlCmd(`update funcionarios
                    set nome='${func.nome}',
                        sexo='${func.sexo}',
                        dataNascimento='${func.dataNascimento}'
                where
                    id= ${id}`,req)
    res.status(201).send(`Atualizado`);
};
exports.delete = (req, res, next) => {
    let id = req.params.id;
    execSqlCmd(`delete from funcionarios where id= ${id}`,req)
    res.status(200).send(`Excluido`);
};