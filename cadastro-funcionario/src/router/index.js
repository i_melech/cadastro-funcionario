import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Home from '@/components/Home'
import CreateUpdateFuncionario from '@/components/CreateUpdateFuncionario'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/Add',
      name: 'CreateUpdateFuncionario',
      component: CreateUpdateFuncionario
    },
    {
      path: '/Edit/:id',
      name: 'CreateUpdateFuncionario',
      component: CreateUpdateFuncionario
    }
  ]
})
